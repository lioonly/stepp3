// Позначки для схову та фільтрації візитів
const searchInput = document.querySelector('.search_filter');
const doctorsFilter = document.getElementById("filter_doctor");
const urgencyFilter = document.getElementById("filter_urgency");

// Список візитів
const visitsData = [
    {
        title: "Візит до кардіолога",
        doctor: "Кардіолог",
        purpose: "Загальне обстеження серця",
        description: "Записався на прийом до кардіолога для перевірки стану серця.",
        status: "Open",
        urgency: "High",
        fullName: "Іванов Іван Іванович",
        age: 40
    },
];

// Функція для створення карточки візиту
function createVisitCard(visit) {
    const card = document.createElement("div");
    card.className = "visit-card";

    const title = document.createElement("h2");
    title.textContent = visit.title;

    const doctor = document.createElement("p");
    doctor.textContent = `Лікар: ${visit.doctor}`;

    const purpose = document.createElement("p");
    purpose.textContent = `Мета візиту: ${visit.purpose}`;

    const description = document.createElement("p");
    description.textContent = `Опис візиту: ${visit.description}`;

    const status = document.createElement("p");
    status.textContent = `Статус: ${visit.status}`;

    const urgency = document.createElement("p");
    urgency.textContent = `Терміновість: ${visit.urgency}`;

    const fullName = document.createElement("p");
    fullName.textContent = `ПІБ: ${visit.fullName}`;

    card.appendChild(title);
    card.appendChild(doctor);
    card.appendChild(purpose);
    card.appendChild(description);
    card.appendChild(status);
    card.appendChild(urgency);
    card.appendChild(fullName);

    return card;
}

// Функція для відображення візитів на сторінці
function displayVisits(visits) {
    const visitsContainer = document.getElementById("visitsContainer");
    visitsContainer.innerHTML = "";

    visits.forEach(visit => {
        const card = createVisitCard(visit);
        visitsContainer.appendChild(card);
    });
}

// Виклик функції для відображення візитів з прикладовими даними
displayVisits(visitsData);

// Функція для фільтрації візитів
function filterVisits() {
    const searchValue = searchInput.value.toLowerCase();
    const selectedDoctorFilter = doctorsFilter.options[doctorsFilter.selectedIndex].value;
    const selectedUrgencyFilter = urgencyFilter.options[urgencyFilter.selectedIndex].value;

    visitsData.forEach(visit => {
        const card = document.querySelector(`.visit-card:contains(${visit.title})`);

        const isDoctor = visit.doctor === selectedDoctorFilter || selectedDoctorFilter === "Лікар";
        const isUrgency = visit.urgency === selectedUrgencyFilter || selectedUrgencyFilter === "Терміновість";
        const isVisible = (
            visit.title.toLowerCase().includes(searchValue) ||
            visit.doctor.toLowerCase().includes(searchValue) ||
            visit.purpose.toLowerCase().includes(searchValue) ||
            visit.description.toLowerCase().includes(searchValue)
        );

        card.classList.toggle('hide', !(isDoctor && isUrgency && isVisible));
    });
}

searchInput.addEventListener('input', filterVisits);
doctorsFilter.addEventListener('change', filterVisits);
urgencyFilter.addEventListener('change', filterVisits);
